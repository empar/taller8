package dam.android.fabricio.u4t8database;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import dam.android.fabricio.u4t8database.data.TodoListDBManager;
import dam.android.fabricio.u4t8database.model.Task;

public class EditTaskActivity extends AppCompatActivity {

    private Task task;

    private EditText etTodoEdit;
    private EditText etToAccomplishEdit;
    private EditText etDescriptionEdit;
    private Spinner spinnerPriorityEdit;
    private Spinner spinnerStatusEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_task);

        setUI();

    }

    private void setUI() {
        this.task = (Task) getIntent().getSerializableExtra("Task");

        this.etTodoEdit = findViewById(R.id.etTodoEdit);
        this.etToAccomplishEdit = findViewById(R.id.etToAccomplishEdit);
        this.etDescriptionEdit = findViewById(R.id.etDescriptionEdit);
        this.spinnerPriorityEdit = findViewById(R.id.spinnerPriorityEdit);

        ArrayAdapter<CharSequence> priorityAdapter = ArrayAdapter.createFromResource(this, R.array.priority, android.R.layout.simple_spinner_item);
        priorityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPriorityEdit.setAdapter(priorityAdapter);
        this.spinnerStatusEdit = findViewById(R.id.spinnerStatusEdit);
        ArrayAdapter<CharSequence> statusAdapter = ArrayAdapter.createFromResource(this, R.array.status, android.R.layout.simple_spinner_item);
        statusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerStatusEdit.setAdapter(statusAdapter);
        this.etTodoEdit.setText(task.getTodo());
        this.etToAccomplishEdit.setText(task.getToAccomplish());
        this.etDescriptionEdit.setText(task.getDescription());

        spinnerPriorityEdit.setSelection(1);
        spinnerStatusEdit.setSelection(1);

    }

    public void onClick(View view) {

        TodoListDBManager todoListDBManager = new TodoListDBManager(this);

        if(view.getId() == R.id.btSaveEdit) {

            if(etTodoEdit.getText().toString().length() > 0) {

                todoListDBManager.update(task.get_id(),
                        etTodoEdit.getText().toString(),
                        etToAccomplishEdit.getText().toString(),
                        etDescriptionEdit.getText().toString(),
                        spinnerPriorityEdit.getSelectedItem().toString(),
                        spinnerStatusEdit.getSelectedItem().toString());

            } else {

                Toast.makeText(this, getText(R.string.task_data_empty), Toast.LENGTH_LONG).show();

            }

        } else if(view.getId() == R.id.btDeleteEdit) {

            todoListDBManager.delete(task.get_id());

        }

        finish();

    }
}