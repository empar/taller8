package dam.android.fabricio.u4t8database;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import dam.android.fabricio.u4t8database.data.TodoListDBManager;
import dam.android.fabricio.u4t8database.model.Task;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private TodoListDBManager todoListDBManager;
    private ArrayList<Task> myTaskList;
    private RecyclerViewClickListener recyclerViewClickListener;


    static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView tvId;
        TextView tvTodo;
        TextView tvToAccomplish;
        TextView tvDescription;
        TextView tvPriority;
        TextView tvStatus;

        Task task;

        RecyclerViewClickListener itemListener;

        public MyViewHolder(View view, RecyclerViewClickListener itemListener) {
            super(view);

            this.tvId = view.findViewById(R.id.tvId);
            this.tvTodo = view.findViewById(R.id.tvTodo);
            this.tvToAccomplish = view.findViewById(R.id.tvToAccomplish);
            this.tvDescription = view.findViewById(R.id.tvDescription);
            this.tvPriority = view.findViewById(R.id.tvPriority);
            this.tvStatus = view.findViewById(R.id.tvStatus);

            this.itemListener = itemListener;

            view.setOnClickListener(this);

        }


        public void bind(Task task) {

            this.tvId.setText(String.valueOf(task.get_id()));
            this.tvTodo.setText(task.getTodo());
            this.tvToAccomplish.setText(task.getToAccomplish());
            this.tvDescription.setText(task.getDescription());
            this.tvPriority.setText(task.getPriority());
            this.tvStatus.setText(task.getStatus());
            this.task = task;

        }

        @Override
        public void onClick(View v) {
            itemListener.recyclerViewItemClicked(this.task);
        }
    }

    public MyAdapter(TodoListDBManager todoListDBManager, RecyclerViewClickListener recyclerViewClickListener) {

        this.todoListDBManager = todoListDBManager;
        this.recyclerViewClickListener = recyclerViewClickListener;

    }

    public void updateData(ArrayList<Task> tasksList) {

        this.myTaskList = tasksList;
        notifyDataSetChanged();

    }

    public void getData() {

        this.myTaskList = todoListDBManager.getTasks();
        notifyDataSetChanged();

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View itemLayout = LayoutInflater.from(parent.getContext()).inflate(R.layout.item, parent, false);

        return new MyViewHolder(itemLayout, recyclerViewClickListener);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder viewHolder, int position) {

        viewHolder.bind(myTaskList.get(position));

    }

    @Override
    public int getItemCount() {
        return myTaskList.size();
    }

}