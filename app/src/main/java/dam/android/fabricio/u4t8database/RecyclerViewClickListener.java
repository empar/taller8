package dam.android.fabricio.u4t8database;


import dam.android.fabricio.u4t8database.model.Task;

public interface RecyclerViewClickListener {

    void recyclerViewItemClicked(Task task);

}