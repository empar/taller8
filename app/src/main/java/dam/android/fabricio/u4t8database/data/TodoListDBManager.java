package dam.android.fabricio.u4t8database.data;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import java.util.ArrayList;

import dam.android.fabricio.u4t8database.model.Task;


public class TodoListDBManager {
    private TodoListDBHelper todoListDBHelper;
    public final static int ALL_TASKS = 1;
    public final static int COMPLETED = 2;
    public final static int NOT_STARTED = 3;
    public final static int IN_PROGRESS = 4;


    public TodoListDBManager(Context context) {

        todoListDBHelper = TodoListDBHelper.getInstance(context);

    }

    public void insert(String todo, String when, String description, String priority, String status) {

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if(sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();
            contentValue.put(TodoListDBContract.Tasks.TODO, todo);
            contentValue.put(TodoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValue.put(TodoListDBContract.Tasks.DESCRIPTION, description);
            contentValue.put(TodoListDBContract.Tasks.PRIORITY, priority);
            contentValue.put(TodoListDBContract.Tasks.STATUS, status);
            sqLiteDatabase.insert(TodoListDBContract.Tasks.TABLE_NAME, null, contentValue);

        }

    }

    public void update(int id, String todo, String when, String description, String priority, String status) {

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if(sqLiteDatabase != null) {
            ContentValues contentValue = new ContentValues();
            contentValue.put(TodoListDBContract.Tasks.TODO, todo);
            contentValue.put(TodoListDBContract.Tasks.TO_ACCOMPLISH, when);
            contentValue.put(TodoListDBContract.Tasks.DESCRIPTION, description);
            contentValue.put(TodoListDBContract.Tasks.PRIORITY, priority);
            contentValue.put(TodoListDBContract.Tasks.STATUS, status);

            sqLiteDatabase.update(TodoListDBContract.Tasks.TABLE_NAME, contentValue, TodoListDBContract.Tasks._ID + "=" + id, null);

        }

    }

    public void delete(int id) {

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if(sqLiteDatabase != null) {

            sqLiteDatabase.delete(TodoListDBContract.Tasks.TABLE_NAME, TodoListDBContract.Tasks._ID + "=" + id, null);

        }

    }

    public void deleteCompleted() {

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if(sqLiteDatabase != null) {

            sqLiteDatabase.delete(TodoListDBContract.Tasks.TABLE_NAME, TodoListDBContract.Tasks.STATUS + "= \"Completed\"", null);

        }

    }

    public void deleteAll() {

        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getWritableDatabase();

        if(sqLiteDatabase != null) {

            sqLiteDatabase.delete(TodoListDBContract.Tasks.TABLE_NAME, null, null);

        }

    }


    public ArrayList<Task> getTaskByStatus(int status) {

        ArrayList<Task> taskList = new ArrayList<>();


        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if(sqLiteDatabase != null) {

            String[] projection = new String[]{TodoListDBContract.Tasks._ID,
                    TodoListDBContract.Tasks.TODO,
                    TodoListDBContract.Tasks.TO_ACCOMPLISH,
                    TodoListDBContract.Tasks.DESCRIPTION,
                    TodoListDBContract.Tasks.PRIORITY,
                    TodoListDBContract.Tasks.STATUS};

            Cursor cursorTodoList = null;

            if(status == NOT_STARTED) {

                cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                        projection,
                        TodoListDBContract.Tasks.STATUS + "= \"Not Started\"",
                        null,
                        null,
                        null,
                        null);

            } else if (status == IN_PROGRESS) {

                cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                        projection,
                        TodoListDBContract.Tasks.STATUS + "= \"In Progress\"",
                        null,
                        null,
                        null,
                        null);

            } else if (status == COMPLETED) {

                cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                        projection,
                        TodoListDBContract.Tasks.STATUS + "= \"Completed\"",
                        null,
                        null,
                        null,
                        null);

            } else if (status == ALL_TASKS) {

                taskList = getTasks();

                return taskList;

            }

            if (cursorTodoList != null) {

                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);


                while(cursorTodoList.moveToNext()) {

                    Task task = new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getString(priorityIndex),
                            cursorTodoList.getString(statusIndex));

                    taskList.add(task);

                }

                cursorTodoList.close();

            }

        }

        return taskList;

    }


    public ArrayList<Task> getTasks() {

        ArrayList<Task> taskList = new ArrayList<>();


        SQLiteDatabase sqLiteDatabase = todoListDBHelper.getReadableDatabase();

        if(sqLiteDatabase != null) {
            String[] projection = new String[]{TodoListDBContract.Tasks._ID,
                    TodoListDBContract.Tasks.TODO,
                    TodoListDBContract.Tasks.TO_ACCOMPLISH,
                    TodoListDBContract.Tasks.DESCRIPTION,
                    TodoListDBContract.Tasks.PRIORITY,
                    TodoListDBContract.Tasks.STATUS};

            Cursor cursorTodoList = sqLiteDatabase.query(TodoListDBContract.Tasks.TABLE_NAME,
                    projection,
                    null,
                    null,
                    null,
                    null,
                    null);


            if (cursorTodoList != null) {


                int _idIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks._ID);
                int todoIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TODO);
                int to_AccomplishIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.TO_ACCOMPLISH);
                int descriptionIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.DESCRIPTION);
                int priorityIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.PRIORITY);
                int statusIndex = cursorTodoList.getColumnIndexOrThrow(TodoListDBContract.Tasks.STATUS);


                while(cursorTodoList.moveToNext()) {

                    Task task = new Task(
                            cursorTodoList.getInt(_idIndex),
                            cursorTodoList.getString(todoIndex),
                            cursorTodoList.getString(to_AccomplishIndex),
                            cursorTodoList.getString(descriptionIndex),
                            cursorTodoList.getString(priorityIndex),
                            cursorTodoList.getString(statusIndex));

                    taskList.add(task);

                }

                cursorTodoList.close();

            }
        }

        return taskList;

    }

    public void close() {

        todoListDBHelper.close();

    }



}