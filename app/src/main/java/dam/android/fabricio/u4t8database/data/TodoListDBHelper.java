package dam.android.fabricio.u4t8database.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class TodoListDBHelper extends SQLiteOpenHelper {

    private static TodoListDBHelper instanceDBHelper;

    public static synchronized TodoListDBHelper getInstance(Context context) {

        if(instanceDBHelper == null) {

            instanceDBHelper = new TodoListDBHelper(context.getApplicationContext());
        }
        return instanceDBHelper;

    }
    private TodoListDBHelper(Context context) {
        super(context, TodoListDBContract.DB_NAME, null, TodoListDBContract.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL(TodoListDBContract.Tasks.CREATE_TABLE);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

        sqLiteDatabase.execSQL(TodoListDBContract.Tasks.DELETE_TABLE);
        onCreate(sqLiteDatabase);

    }
}