package dam.android.fabricio.u4t8database;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;

import android.view.Menu;
import android.view.MenuItem;

import dam.android.fabricio.u4t8database.data.TodoListDBManager;
import dam.android.fabricio.u4t8database.model.Task;


public class MainActivity extends AppCompatActivity implements RecyclerViewClickListener {

    private RecyclerView rvTodoList;
    private TodoListDBManager todoListDBManager;
    private MyAdapter myAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        todoListDBManager = new TodoListDBManager(this);
        myAdapter = new MyAdapter(todoListDBManager, this);

        setUI();

    }

    private void setUI() {

        // ActionBar
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // set fab: opens an activity to ADD A NEW TASK to the DB
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // start activity to add a new record to our table
                startActivity(new Intent(getApplicationContext(), AddTaskActivity.class));

            }
        });

        // set recyclerView
        rvTodoList = findViewById(R.id.rvTodoList);
        rvTodoList.setHasFixedSize(true);
        rvTodoList.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        rvTodoList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
        rvTodoList.setAdapter(myAdapter);

    }

    @Override
    public void onResume() {
        super.onResume();

        myAdapter.getData();

    }

    @Override
    public void onDestroy() {

        todoListDBManager.close();

        super.onDestroy();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.action_show_all:
                myAdapter.updateData(todoListDBManager.getTaskByStatus(TodoListDBManager.ALL_TASKS));
                break;
            case R.id.action_show_completed:
                myAdapter.updateData(todoListDBManager.getTaskByStatus(TodoListDBManager.COMPLETED));
                break;
            case R.id.action_show_in_progress:
                myAdapter.updateData(todoListDBManager.getTaskByStatus(TodoListDBManager.IN_PROGRESS));
                break;

            case R.id.action_show_not_started:
                myAdapter.updateData(todoListDBManager.getTaskByStatus(TodoListDBManager.NOT_STARTED));
                break;

            case R.id.action_delete_completed:
                todoListDBManager.deleteCompleted();
                myAdapter.getData();
                break;
            case R.id.action_delete_all:
                todoListDBManager.deleteAll();
                myAdapter.getData();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void recyclerViewItemClicked(Task task) {
        Intent intent = new Intent(getApplicationContext(), EditTaskActivity.class);
        intent.putExtra("Task", task);
        startActivity(intent);

    }
}